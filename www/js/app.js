/* global FormView, SurveyView, ConfigView, HomeView, User, i18next, jqueryI18next */

var App = function() {
  this.form = new FormView();
  this.survey = new SurveyView();
  this.config = new ConfigView();
  this.home = new HomeView();
  this.user = User.get();
};

App.prototype.init = function() {
  var self = this;

  $('#left-panel').panel();
  $('#left-panel ul[data-role="listview"]').listview();
  $('[data-role="header"]').toolbar();

  $('a[data-function="show-home"]').click(function() {
    self.home.show();
  });

  $('a[data-function="list-forms"]').click(function() {
    self.form.list();
  });

  $('a[data-function="save-survey"]').click(function() {
    self.survey.save();
  });

  $('a[data-function="finish-survey"]').click(function() {
    self.survey.finish();
  });

  $('a[data-function="list-surveys"]').click(function() {
    self.survey.list();
  });

  $('a[data-function="send-surveys"]').click(function() {
    self.survey.list(SurveyView.SEND_ACTION);
  });

  $('a[data-function="delete-survey"]').click(function() {
    self.survey.list(SurveyView.DELETE_ACTION);
  });

  $('a[data-function="export-surveys"]').click(function() {
    self.survey.export();
  });

  $('a[data-function="show-config"]').click(function() {
    self.config.show();
  });

  $('a[data-function="save-config"]').click(function() {
    self.config.save();
  });

  $('a[data-function="cancel-config"]').click(function() {
    self.home.show();
  });

  User.getLanguage().then(function(language) {
    App.changeLanguage(language).then(function() {
      self.home.show();
    });
  });

  document.addEventListener('backbutton', function(event) {
    if ($.mobile.activePage.is('#home')) {
      navigator.notification.confirm(
        (i18next.t('exit-app-message')),
        function(button) {
          if (button === 1) {
            event.preventDefault();
            navigator.app.exitApp();
          }
        }, i18next.t('app-name'), i18next.t('exit-button-confirm') + ',' + i18next.t('exit-button-decline'));
    }
  }, false);
};

App.changeLanguage = function(language) {
  return new Promise(function(resolve) {
    i18next.changeLanguage(language.substr(0, 2), function() {
      App.localizeAppContent();
      moment.locale(language.substr(0, 2));
      resolve();
    });
  });
};

App.localizeAppContent = function() {
  $('#home').localize();
  $('#left-panel').localize();
  $('#right-panel').localize();
  $('#config').localize();
  $('#survey-fill').localize();
};

App.prototype.closeLeftPanel = function() {
  $('#left-panel').panel('close');
};

App.prototype.error = function(error) {
  navigator.notification.alert(error.message, null, 'Error');
  console.error(error);
};

App.prototype.setHeader = function(text) {
  $('.ui-header .ui-title').text(text);
};

var app = new App();

$(document).on('deviceready', function() {
  app.init();
  window.onerror = function(message, source, lineno, colno, error) {
    app.error(error);
  };
});

// Setup Handlebars
Handlebars.registerHelper('getCollectionFieldValue', function(collection, index, field) {
  return collection.value[index][field.name];
});

Handlebars.registerHelper('getCollectionId', function(collection, index) {
  return collection.value[index]['id'];
});

Handlebars.registerHelper('getChecked', function(fieldValue, optionValue) {
  if ($.isArray(fieldValue)) {
    return $.inArray(String(optionValue), fieldValue) >= 0 ? 'checked' : null;
  }
  return String(fieldValue) === String(optionValue) ? 'checked' : null;
});

Handlebars.registerHelper('getSelected', function(fieldValue, optionValue) {
  return fieldValue == optionValue ? 'selected' : null; // eslint-disable-line eqeqeq
});

Handlebars.registerHelper('getFieldDependencies', function(field, option) {
  return field.dependencies && (!field.multiple
    || field.dependencies.value == option.value); // eslint-disable-line eqeqeq
});

Handlebars.registerHelper('momentCalendar', function(date) {
  return moment(date).calendar();
});

Handlebars.registerHelper('momentFromNow', function(date) {
  return moment(date).fromNow();
});

Handlebars.registerHelper('t', function(i18Key) {
  var result = i18next.t(i18Key);
  return new Handlebars.SafeString(result);
});

Handlebars.partials = Handlebars.templates;

// Setup i18next
i18next.use(window.i18nextXHRBackend).init({
  debug: false,
  lng: window.navigator.userLanguage || window.navigator.language.substr(0, 2) || 'es',
  fallbackLng: 'es',
  backend: {
    loadPath: 'locales/{{lng}}.json'
  }
}, function() {
  jqueryI18next.init(i18next, $);
});

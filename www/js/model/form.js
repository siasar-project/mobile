/* global Store, Api, FormParser, i18next */

var Form = function(data) {
  if (data) {
    this.id = data.id;
    this.label = data.label;
    this.sections = data.sections;
    this.synchronized = data.synchronized;
  }
};

Form.list = function() {
  var forms = Store.getInstance().getForms();
  if (forms) {
    forms = forms.map(function(form) {
      return new Form(form);
    });
  }
  return forms;
};

Form.save = function(forms) {
  return new Promise(function(resolve) {
    var savePromises = [];
    forms.forEach(function(form) {
      savePromises.push(form.save());
    });
    Promise.all(savePromises).then(function(result) {
      Store.getInstance().saveForms(forms);
      resolve(result);
    });
  });
};

Form.get = function(id) {
  return new Promise(function(resolve, reject) {
    Store.getInstance().getForm(id).then(function(form) {
      if (form) {
        form = new Form(form);
        form.fieldDependencies = Form.getFieldDependencies();
      }
      resolve(form);
    }).catch(function(error) {
      reject(error);
    });
  });
};

Form.download = function(log) {
  return new Promise(function(resolve, reject) {
    var downloadLog = log.add(i18next.t('getting-forms-list-message'));
    Api.getFormsList().then(function(data) {
      var forms = $.map(data, function(value, index) {
        return new Form({
          id: index,
          label: value,
          synchronized: false
        });
      });
      Form.save(forms).then(function() {
        log.remove(downloadLog);
        resolve(forms);
      });
    }).catch(function(error) {
      reject(error);
    });
  });
};

Form.downloadFields = function(form, log) {
  return new Promise(function(resolve, reject) {
    var downloadLog = log.add(i18next.t('downloading-fields-message') + ' "' + form.label + '"...');
    Api.getFormFields(form.id).then(function(fields) {
      log.remove(downloadLog);
      resolve(fields);
    }).catch(function(error) {
      reject(error);
    });
  });
};

Form.downloadGroupStructure = function(log) {
  return new Promise(function(resolve, reject) {
    var downloadLog = log.add(i18next.t('downloading-structures-message'));
    Api.getGroupStructure().then(function(structure) {
      log.remove(downloadLog);
      resolve(structure);
    }).catch(function(error) {
      reject(error);
    });
  });
};

Form.downloadFieldCollections = function(log) {
  return new Promise(function(resolve, reject) {
    var downloadLog = log.add(i18next.t('downloading-collections-message'));
    Api.getFieldCollections().then(function(collections) {
      log.remove(downloadLog);
      resolve(collections);
    }).catch(function(error) {
      reject(error);
    });
  });
};

Form.synchronize = function(log) {
  Store.getInstance().clearForms();
  return new Promise(function(resolve, reject) {
    Promise.all([Form.download(log), Form.downloadFieldCollections(log),
      Form.downloadGroupStructure(log), Form.downloadFieldDependencies(log)]).then(function(data) {
        var fieldsPromises = data[0].map(function(form) {
          return Form.downloadFields(form, log);
        });
        Promise.all(fieldsPromises).then(function(fieldsData) {
          var cache = [];
          var parsersPromises = data[0].map(function(form, index) {
            var parser = new FormParser(form, fieldsData[index], data[1], data[2], data[3],
              cache, log);
            return parser.parse();
          });
          Promise.all(parsersPromises).then(function(forms) {
            forms.forEach(function(form) {
              form.synchronized = true;
            });
            var saveLog = log.add(i18next.t('saving-forms-message'));
            Form.save(forms).then(function() {
              log.remove(saveLog);
              resolve(forms);
            });
          }).catch(function(error) {
            reject(error);
          });
        });
      }).catch(function(error) {
        reject(error);
      });
  });
};

Form.getShs = function(id) {
  return Store.getInstance().getShs(id);
};

Form.downloadFieldDependencies = function(log) {
  var fieldDependencies = {};

  function parseFieldConfig(fieldName, fieldConfig) {
    if (fieldConfig.options.condition !== 'value' || !fieldConfig.options.value.length) {
      return;
    }
    if (!fieldDependencies[fieldConfig.dependee]) {
      fieldDependencies[fieldConfig.dependee] = {
        fields: [],
        visible: fieldConfig.options.state === 'visible',
        value: fieldConfig.options.value && fieldConfig.options.value[0] ?
          fieldConfig.options.value[0].tid || fieldConfig.options.value[0].value : null
      };
    }
    fieldDependencies[fieldConfig.dependee].fields.push(fieldName);
  }

  return new Promise(function(resolve, reject) {
    var downloadLog = log.add(i18next.t('downloading-dependencies-message'));
    Api.getFieldDependencies().then(function(dependencies) {
      log.remove(downloadLog);
      $.each(dependencies.entityform, function(form, fields) {
        $.each(fields, parseFieldConfig);
      });
      $.each(dependencies.field_collection_item, function(collection, fields) {
        $.each(fields, parseFieldConfig);
      });
      Store.getInstance().saveFieldDependencies(fieldDependencies);
      resolve(fieldDependencies);
    }).catch(function(error) {
      reject(error);
    });
  });
};

Form.getFieldDependencies = function() {
  return Store.getInstance().getFieldDependencies();
};

Form.prototype.save = function() {
  return Store.getInstance().saveForm(this);
};

Form.prototype.parse = function(fieldFunction, collectionFunction, collectionFieldFunction) {
  var self = this;
  $.each(self.sections, function(sectionIndex) {
    self.parseGroup(self.sections[sectionIndex].children, fieldFunction, collectionFunction,
      collectionFieldFunction);
  });
};

Form.prototype.parseGroup = function(group, fieldFunction, collectionFunction,
  collectionFieldFunction) {
  var self = this;
  $.each(group, function(childIndex, child) {
    switch (child.type) {
      case 'group':
        self.parseGroup(group[childIndex].children, fieldFunction, collectionFunction,
        collectionFieldFunction);
        break;
      case 'field_collection_embed':
        if (collectionFunction) {
          collectionFunction(group[childIndex]);
        }
        if (collectionFieldFunction) {
          self.parseGroup(group[childIndex].collection, collectionFieldFunction, collectionFunction,
            collectionFieldFunction);
        } else {
          self.parseGroup(group[childIndex].collection, fieldFunction, collectionFunction,
          collectionFieldFunction);
        }
        break;
      default:
        if (fieldFunction) {
          fieldFunction(group[childIndex]);
        }
    }
  });
};

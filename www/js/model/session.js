/* global Store, Api, User, UserNotDefinedError */

var Session = function(data) {
  if (data) {
    this.sessionName = data.sessionName;
    this.sessionId = data.sessionId;
    this.token = data.token;
    this.user = data.user;
    this.timestamp = data.timestamp;
    this.server = data.server;
  }
};

Session.create = function(force) {
  if (force || typeof Session.pending === 'undefined' || Session.pending === false) {
    Session.promise = new Promise(function(resolve, reject) {
      Session.pending = true;

      var user = User.get();
      if (!user) {
        reject(new UserNotDefinedError());
        return;
      }

      var userData = {
        name: user.username,
        pass: user.password
      };

      var session = Session.get();

      if (!session) {
        Api.login(userData).then(function(data) {
          session = Session.init(data);
          Session.pending = false;
          resolve(session);
        }).catch(function(error) {
          reject(error);
          Session.pending = false;
        });
      } else if (session.server === user.server) {
        Api.logout().then(function() {
          Api.login(userData).then(function(data) {
            session = Session.init(data);
            Session.pending = false;
            resolve(session);
          }).catch(function(error) {
            reject(error);
            Session.pending = false;
          });
        });
      } else {
        Api.logout().then(function() {
          session.server = user.server;
          session.save();
          Api.login(userData).then(function(data) {
            session = Session.init(data);
            Session.pending = false;
            resolve(session);
          }).catch(function(error) {
            reject(error);
            Session.pending = false;
          });
        });
      }
    });
  }
  return Session.promise;
};

Session.init = function(data) {
  var user = User.get();
  user.id = data.user.uid;
  user.email = data.user.mail;
  user.language = data.user.language;
  user.name = data.user.field_nombre_completo.length ?
    data.user.field_nombre_completo[user.language][0].safe_value : data.user.name;
  user.country = data.user.field_pais.und[0].iso2;
  Object.getOwnPropertyNames(data.user.roles).forEach(function(role) {
    user.admin = data.user.roles[role] === 'administrator';
  });
  user.save();
  var session = new Session({
    server: user.server,
    sessionId: data.sessid,
    sessionName: data.session_name,
    token: data.token,
    user: user,
    timestamp: Date.now()
  });
  document.cookie = session.getCookieString();
  session.save();
  return session;
};

Session.get = function() {
  var session = Store.getInstance().getSession();
  if (session) {
    session = new Session(session);
    session.user = new User(session.user);
  }
  return session;
};

Session.exists = function() {
  return Store.getInstance().getSession() !== null;
};

Session.prototype.save = function() {
  Store.getInstance().saveSession(this);
};

Session.prototype.getCookieString = function() {
  return this.sessionName + '=' + this.sessionId;
};

Session.prototype.isValid = function() {
  return (Date.now() - this.timestamp) < 600000;
};

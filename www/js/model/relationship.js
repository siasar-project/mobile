/* global Store */

var Relationship = function(data) {
  if (data) {
    this.parentId = parseInt(data.parentId, 10);
    this.childId = parseInt(data.childId, 10);
    this.collectionName = data.collectionName;
    this.fieldName = data.fieldName;
    this.index = data.index;
  }
};

Relationship.list = function() {
  var relations = Store.getInstance().getRelationships();
  return relations.map(function(relation) {
    return new Relationship(relation);
  });
};

Relationship.prototype.save = function() {
  var self = this;
  var exists = false;
  var relations = Relationship.list();
  relations.forEach(function(relation) {
    if (self.isEqual(relation)) {
      relation.parentId = self.parentId;
      exists = true;
    }
  });
  if (!exists) {
    relations.push(this);
  }
  Store.getInstance().saveRelationships(relations);
};

Relationship.getChildren = function(surveyId) {
  var children = {};
  Relationship.list().forEach(function(relation) {
    if (relation.parentId === surveyId) {
      if (!children[relation.childId]) {
        children[relation.childId] = relation;
      }
      if (relation.collectionName) {
        if (!children[relation.childId].indexes) {
          children[relation.childId].indexes = [];
        }
        children[relation.childId].indexes.push(relation.index);
      }
    }
  });
  return Object.keys(children).map(function(child) {
    return children[child];
  });
};

Relationship.getParents = function(surveyId) {
  var relations = Relationship.list();
  return relations.filter(function(relation) {
    return relation.childId === surveyId;
  });
};

Relationship.prototype.delete = function() {
  var self = this;
  var relations = Relationship.list();
  relations.forEach(function(relation, index) {
    if (self.isEqual(relation)) {
      relations.splice(index, 1);
    }
  });
  Store.getInstance().saveRelationships(relations);
};

Relationship.deleteChildren = function(surveyId) {
  var relations = Relationship.list();
  for (var i = 0; i < relations.length; i += 1) {
    if (relations[i].parentId === surveyId) {
      relations.splice(i, 1);
      i -= 1;
    }
  }
  Store.getInstance().saveRelationships(relations);
};

Relationship.prototype.isEqual = function(relation) {
  if (relation.childId === this.childId
    && relation.collectionName === this.collectionName
    && relation.fieldName === this.fieldName
    && relation.index === this.index) {
    return true;
  }
  return false;
};

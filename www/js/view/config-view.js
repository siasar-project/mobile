/* global Session, User, App, i18next */

var ConfigView = function() {
  this.enableServerCount = 0;
};

ConfigView.prototype.show = function() {
  app.closeLeftPanel();

  this.user = app.user;
  var content = $('#config div.ui-content');
  var self = this;

  content.html(Handlebars.templates['config']({
    user: self.user
  }));

  content.find('a[data-function="sync-forms"]').click(function() {
    navigator.notification.confirm(i18next.t('synchronize-alert') + ' ' + i18next.t('confirmation-question'), function(button) {
      if (button === 1) {
        app.form.synchronize();
      }
    }, i18next.t('synchronize-header'));
  });

  content.find('a[data-function="login"]').click(function() {
    navigator.notification.confirm(i18next.t('synchronize-trigger') + ' ' + i18next.t('synchronize-alert') + ' ' + i18next.t('confirmation-question'), function(button) {
      if (button === 1) {
        self.login();
      }
    }, i18next.t('synchronize-header'));
  });

  content.find('#autosave').change(function() {
    self.user.autosave = $(this).is(':checked');
    self.user.save();
  });

  content.find('#user-config .ui-bar').on('tap', function() {
    self.enableServerCount += 1;
    if (self.enableServerCount >= 5) {
      $('#server').selectmenu('enable');
    }
    clearTimeout(self.enableServerTimeout);
    self.enableServerTimeout = setTimeout(function () {
      self.enableServerCount = 0;
    }, 1000);
  });

  content.find('form').trigger('create');

  app.setHeader(i18next.t('configuration-header'));
  $.mobile.navigate('#config');
};

ConfigView.prototype.login = function() {
  $.mobile.loading('show');

  this.user = this.user || new User();
  this.user.server = $('#server').val();
  this.user.username = $('#username').val();
  this.user.password = $('#password').val();
  this.user.save();

  Session.create(true).then(function(session) {
    App.changeLanguage(session.user.language).then(function() {
      $.mobile.loading('hide');
      app.user = session.user;
      app.form.synchronize();
    });
  }).catch(function(error) {
    app.error(error);
    $.mobile.loading('hide');
  });
};

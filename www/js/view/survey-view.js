/* global Survey, Form, Relationship, i18next, RequiredError, OutOfRangeError, NotValidValueError */

var SurveyView = function() {
};

SurveyView.EDIT_ACTION = 'edit';
SurveyView.SEND_ACTION = 'send';
SurveyView.DELETE_ACTION = 'delete';

SurveyView.prototype.save = function(stay) {
  if (!stay) {
    this.clearPage();
  }
  var self = this;
  self.getData();
  self.survey.save().then(function() {
    if (!stay) {
      navigator.notification.alert(i18next.t('save-survey-success-alert'), null, i18next.t('save-survey-header'));
      self.list();
    }
  }).catch(function(error) {
    navigator.notification.alert(i18next.t('save-survey-fail-alert') + '\n\n' + error, null, i18next.t('save-survey-header'));
    app.error(error);
  });
};

SurveyView.prototype.finish = function() {
  this.clearPage();
  var self = this;
  try {
    $('*').removeClass('error');
    self.getData(true);
    self.survey.finish().then(function() {
      navigator.notification.alert(i18next.t('finish-survey-success-alert'), null, i18next.t('finish-survey-header'));
      self.list();
    }).catch(function(error) {
      app.error(error);
    });
  } catch (error) {
    error.element.addClass('error');
    error.element.closest('.section').collapsible('expand');
    if (typeof error.element.offset() !== 'undefined') {
      $.mobile.silentScroll(error.element.offset().top - 100);
    } else {
      $.mobile.silentScroll(-40);
    }
    navigator.notification.alert(error.message, null, error.title);
  }
};

SurveyView.prototype.fieldDependency = function(field, container) {
  container = container || $('#survey-fill div.ui-content');
  var dependency = this.survey.form.fieldDependencies[$(field).data('name')];

  dependency.fields.forEach(function(targetField) {
    var target = container.find('div[data-id=' + targetField + ']');
    if (field.checked && $(field).data('value') == dependency.value && dependency.visible) {  // eslint-disable-line eqeqeq
      target.show();
    } else {
      target.hide();
    }
  });
};

SurveyView.prototype.open = function(formId, surveyId) {
  var self = this;

  Survey.get(surveyId).then(function(survey) {
    Form.get(formId).then(function(form) {
      if (!survey) {
        form.parse(function(field) {
          if (field.name === 'field_pais') {
            field.value = app.user.country;
          } else if (field.name === 'field_user_reference') {
            field.value = app.user.id;
          }
        });
        survey = new Survey({
          form: form
        });
      }
      self.survey = survey;
      var references = {};
      Survey.list(true).then(function(surveys) {
        surveys.forEach(function(surveyOption) {
          if (!references[surveyOption.form.id]) {
            references[surveyOption.form.id] = [];
          }
          references[surveyOption.form.id].push({
            value: surveyOption.id,
            label: surveyOption.name
          });
        });

        var content = $('#survey-fill div.ui-content');
        content.html(Handlebars.templates['survey']({ // eslint-disable-line dot-notation
          survey: survey,
          references: references
        }));

        content.find('div[data-role="collapsibleset"]').collapsibleset();
        content.find('form').trigger('create');

        content.find('.ui-collapsible-heading').click(function() {
          $.mobile.silentScroll(this);
        });

        content.find('a[data-function="get-geolocation"]').click(function() {
          self.getGeolocation($(this).parent());
        });

        content.find('a[data-function="get-picture"]').click(function() {
          self.getPicture($(this).parent());
        });

        content.find('a[data-function="remove-picture"]').click(function() {
          self.removePicture($(this).parent());
        });

        content.find('a[data-function="add-collection"]').click(function() {
          self.addCollection($(this).parents('fieldset'), references);
        });

        content.find('a[data-function="remove-collection"]').click(function() {
          self.removeCollection($(this).parent());
        });

        $.each($('a[data-function="remove-collection"]'), function(index, button) {
          if ($(button).siblings('.collection-group').length > 1) {
            $(button).show();
          }
        });

        content.find('a[data-function="get-shs"]').click(function() {
          self.getShs($('div').find('[data-id="' + $(this).data('target') + '"]'), 0);
        });

        content.find('a[data-function="clear-shs"]').click(function() {
          self.clearShs($('div').find('[data-id="' + $(this).data('target') + '"]'));
        });

        content.find('[data-dependency="true"]').each(function() {
          var context = $(this).parents('.collection-group').length ? $(this).parents('.collection-group') : null;
          self.fieldDependency(this, context);
          $(this).change(function() {
            self.fieldDependency(this, context);
          });
        });

        $('#survey-fill div[data-role="footer"]').show();

        $(':mobile-pagecontainer').on('pagecontainerbeforechange', function(event, ui) {
          if (ui.prevPage.attr('id') === 'survey-fill' && typeof ui.toPage === 'object' &&
            ui.toPage.attr('id') !== 'shs-select') {
            event.preventDefault();
            if (self.saveInterval) {
              self.save();
            } else {
              navigator.notification.confirm(
                i18next.t('exit-survey-message'), function(button) {
                  if (button === 1) {
                    self.clearPage();
                    $.mobile.navigate('#' + ui.toPage.attr('id'));
                  }
                }, i18next.t('app-name'));
            }
          }
        });

        $.mobile.navigate('#survey-fill');

        if (app.user.autosave === true) {
          self.saveInterval = setInterval(function() {
            self.save(true);
          }, 10000);
        } else if (typeof app.user.autosave === 'undefined') {
          self.saveTimeout = setTimeout(function() {
            navigator.notification.confirm(
              i18next.t('autosave-message'), function(button) {
                if (button === 1) {
                  app.user.autosave = true;
                  self.save(true);
                  self.saveInterval = setInterval(function() {
                    self.save(true);
                  }, 10000);
                } else {
                  app.user.autosave = false;
                }
                app.user.save();
              }, i18next.t('app-name'));
          }, 10000);
        }
      }).catch(function(error) {
        app.error(error);
      });
    });
  });
};

SurveyView.prototype.list = function(action) {
  var self = this;
  app.closeLeftPanel();
  var params = {};
  var actionFunction;

  if (action === SurveyView.SEND_ACTION) {
    params.finished = true;
  }
  Survey.list(params).then(function(surveys) {
    var content = $('#surveys-list div.ui-content');
    var emptyMessage = i18next.t('no-saved-survey-message');

    switch (action) {
      case SurveyView.SEND_ACTION:
        app.setHeader(i18next.t('send-survey-header'));
        emptyMessage = i18next.t('no-finished-survey-message');
        actionFunction = function() {
          self.send($(this).data('id'));
        };
        break;
      case SurveyView.DELETE_ACTION:
        app.setHeader(i18next.t('delete-survey-header'));
        actionFunction = function() {
          self.delete($(this).data('id'));
        };
        break;
      default:
        app.setHeader(i18next.t('edit-survey-header'));
        actionFunction = function() {
          self.open($(this).data('form-id'), $(this).data('id'));
        };
    }

    content.html(Handlebars.templates['surveys-list']({
      surveys: surveys,
      emptyMessage: emptyMessage
    }));
    content.find('ul[data-role="listview"]').listview();

    content.find('ul[data-role="listview"] li a').click(actionFunction);

    $.mobile.navigate('#surveys-list');
  }).catch(function(error) {
    app.error(error);
  });
};

SurveyView.prototype.delete = function(id) {
  var self = this;
  navigator.notification.confirm(i18next.t('delete-survey-alert'), function(button) {
    if (button === 1) {
      Survey.get(id).then(function(survey) {
        survey.delete();
        navigator.notification.alert(i18next.t('delete-survey-success-alert'), null, i18next.t('delete-survey-header'));
        self.list(SurveyView.DELETE_ACTION);
      }).catch(function(error) {
        app.error(error);
      });
    } else {
      self.list(SurveyView.DELETE_ACTION);
    }
  }, i18next.t('delete-survey-header'));
};

SurveyView.prototype.export = function() {
  app.closeLeftPanel();
  Survey.list().then(function(surveys) {
    if (!(cordova.file.externalDataDirectory || cordova.file.documentsDirectory)) {
      throw new Error(i18next.t('no-filesystem-access-allowed'));
    }
    window.resolveLocalFileSystemURL((cordova.file.externalDataDirectory ||
      cordova.file.documentsDirectory), function(dirEntry) {
      var fileName = moment().format('YYYYMMDDHHmmSS') + '-siasapp-surveys.json';
      dirEntry.getFile(fileName, { create: true, exclusive: false }, function(fileEntry) {
        fileEntry.createWriter(function(fileWriter) {
          surveys.forEach(function(survey) {
            survey.form = null;
          });
          fileWriter.write(JSON.stringify(surveys));
          navigator.notification.alert(i18next.t('export-survey-success-alert') + '\n\n' +
            fileEntry.nativeURL, null, i18next.t('export-survey-header'));
        }, function() {
          throw new Error(i18next.t('writing-file-error'));
        });
      }, function() {
        throw new Error(i18next.t('opening-file-error'));
      });
    }, function() {
      throw new Error(i18next.t('opening-filesistem-error'));
    });
  }).catch(function(error) {
    app.error(error);
  });
};

SurveyView.prototype.getData = function(validate) {
  function getGeoFieldValue(field, context) {
    var latitude = context.find('input[name^="' + field.name + '"][name*="_latitude"]').val();
    var longitude = context.find('input[name^="' + field.name + '"][name*="_longitude"]').val();

    return {
      latitude: latitude !== '' ? latitude : null,
      longitude: longitude !== '' ? longitude : null
    };
  }

  function getOptionButtonsValue(field, context) {
    var value = $.map(context.find('[name*="' + field.name + '"]:checked'), function(option) {
      return $(option).data('value').toString();
    });
    if (!field.multiple) {
      if (value.length === 1) {
        value = value[0];
      } else {
        value = null;
      }
    }
    return value;
  }

  function getImageValue(field, context) {
    var value = context.find('[name*="' + field.name + '"]').attr('src');
    if (value === '') {
      return null;
    }
    return value;
  }

  function getNumberValue(field, context) {
    var value = context.find('[name*="' + field.name + '"]').val();
    if (value === '') {
      return null;
    }
    return Number(value);
  }

  function getTextValue(field, context) {
    var value = context.find('[name*="' + field.name + '"]').val();
    if (!value || value === '') {
      return null;
    }
    return value.trim();
  }

  function getMVFValue(field, context) {
    var value = getNumberValue(field, context);
    if (!value) {
      return null;
    }
    return {
      value: value,
      unit: context.find('[name*="' + field.name + '_unit"]').val()
    };
  }

  function getShsValue(field, context) {
    var value = getTextValue(field, context);
    if (!value) {
      return null;
    }
    return {
      value: value,
      label: context.find('input[name*="' + field.name + '_label"]').val()
    };
  }

  function validateField(field, value, context) {
    var element = context.find('[name*="' + field.name + '"]');

    function validateOptionsButtons() {
      if (field.required) {
        if (field.multiple && value.length === 0) {
          throw new RequiredError(field, element);
        }
      }
    }

    function validateGeoField() {
      if (field.required && value.latitude === null) {
        element = context.find('input[name^="' + field.name + '"][name*="_latitude"]');
        throw new RequiredError(field, element);
      } else if (field.latitude !== null) {
        var latitude = parseFloat(value.latitude);
        if (isNaN(value.latitude) || latitude < -90 || latitude > 90) {
          element = context.find('input[name^="' + field.name + '"][name*="_latitude"]');
          throw new NotValidValueError(field, element);
        }
      }

      if (field.required && value.longitude === null) {
        element = context.find('input[name^="' + field.name + '"][name*="_longitude"]');
        throw new RequiredError(field, element);
      } else if (field.longitude !== null) {
        var longitude = parseFloat(value.longitude);
        if (isNaN(value.longitude) || longitude < -180 || longitude > 180) {
          element = context.find('input[name^="' + field.name + '"][name*="_longitude"]');
          throw new NotValidValueError(field, element);
        }
      }
    }

    function validateRangeField() {
      if ((field.range.min !== null && value < field.range.min) ||
        (field.range.max !== null && value > field.range.max)) {
        throw new OutOfRangeError(field, element);
      }
    }

    if (field.required && value === null) {
      throw new RequiredError(field, element);
    }

    if (field.type === 'geofield_latlon') {
      validateGeoField();
    }

    if (field.type === 'options_buttons') {
      validateOptionsButtons();
    }

    if (field.range) {
      validateRangeField();
    }
  }

  function getFieldValue(field, context) {
    switch (field.type) {
      case 'geofield_latlon':
        return getGeoFieldValue(field, context);
      case 'options_buttons':
        return getOptionButtonsValue(field, context);
      case 'image_image':
        return getImageValue(field, context);
      case 'number':
        return getNumberValue(field, context);
      case 'mvf_widget_default':
        return getMVFValue(field, context);
      case 'siasar_hierarchical_select':
        return getShsValue(field, context);
      default:
        return getTextValue(field, context);
    }
  }

  var formElement = $('#survey-fill form');
  var self = this;
  self.survey.data = {};

  this.survey.form.parse(function(field) {
    self.survey.data[field.name] = getFieldValue(field, formElement);
    if (validate) {
      validateField(field, self.survey.data[field.name], formElement);
    }
  }, function(collection) {
    self.survey.data[collection.name] = [];
    $.each($('div.collection-group[data-id="' + collection.name + '"]'), function(index, collectionItem) {
      self.survey.data[collection.name].push({
        index: collectionItem.dataset['index']
      });
    });
  }, function(field) {
    $.each(self.survey.data[field.collectionName], function(index) {
      var collection = $($('div.collection-group[data-id="' + field.collectionName + '"]')[index]);
      self.survey.data[field.collectionName][index][field.name] = getFieldValue(field, collection);
      if (validate) {
        validateField(field, self.survey.data[field.collectionName][index][field.name], collection);
      }
    });
  });
};

SurveyView.prototype.send = function(id) {
  if (Relationship.getParents(id).length) {
    navigator.notification.alert(i18next.t('relations-error-message'), null, i18next.t('relations-error-title'));
    return;
  }
  var self = this;
  $.mobile.loading('show');
  Survey.get(id).then(function(survey) {
    survey.send().then(function() {
      navigator.notification.alert(i18next.t('send-survey-success-alert'), null, i18next.t('send-survey-header'));
      $.mobile.loading('hide');
      self.list(SurveyView.SEND_ACTION);
    }).catch(function(error) {
      $.mobile.loading('hide');
      app.error(error);
    });
  }).catch(function(error) {
    app.error(error);
    $.mobile.loading('hide');
  });
};

SurveyView.prototype.addCollection = function(fieldset, references) {
  var self = this;
  var fieldName = fieldset.data('field');
  var fieldCollection;

  this.survey.form.parse(null,
    function(collection) {
      if (collection.name === fieldName) {
        fieldCollection = collection;
        fieldCollection.index = Date.now();
      }
    }, null);

  var content = fieldset.find('.collection-add');
  content.append(Handlebars.templates.field_collection_add({
    fieldCollection: fieldCollection,
    references: references
  }));
  content.trigger('create');

  var collection = fieldset.find('.collection-group:last');

  fieldset.find('a[data-function="remove-collection"]').show();
  fieldset.find('a[data-function="get-geolocation"]').click(function() {
    self.getGeolocation($(this).parent());
  });
  fieldset.find('a[data-function="get-picture"]').click(function() {
    self.getPicture($(this).parent());
  });
  fieldset.find('a[data-function="remove-picture"]').click(function() {
    self.removePicture($(this).parent());
  });
  collection.find('input[data-dependency="true"]').each(function() {
    self.fieldDependency(this, collection);
    $(this).change(function() {
      self.fieldDependency(this, collection);
    });
  });
};

SurveyView.prototype.removeCollection = function(fieldset) {
  var collections = fieldset.find('div.collection-group');
  var index = collections.length - 1;

  $(collections[index]).remove();
  if (index <= 0) {
    fieldset.find('a[data-function="remove-collection"]').hide();
  }
};

SurveyView.prototype.getPicture = function(field) {
  var image = field.find('img');
  navigator.camera.getPicture(function(picture) {
    if (picture.substring(0, 4) === 'file') {
      image.attr('src', picture);
    } else {
      image.attr('src', 'data:image/jpeg;base64,' + picture);
    }
    image.show();
    field.find('a[data-function="remove-picture"]').show();
  }, function(error) {
    app.error(error);
  }, {
    destinationType: navigator.camera.DestinationType.DATA_URL,
    sourceType: navigator.camera.PictureSourceType.CAMERA,
    saveToPhotoAlbum: true
  });
};

SurveyView.prototype.removePicture = function(field) {
  var image = field.find('img');
  image.attr('src', '');
  image.hide();
  field.find('a[data-function="remove-picture"]').hide();
};

SurveyView.prototype.getGeolocation = function(field) {
  cordova.plugins.diagnostic.isLocationEnabled(function(enabled) {
    if (enabled) {
      $.mobile.loading('show');
      navigator.geolocation.getCurrentPosition(function(position) {
        field.find('input[name$="_latitude"]').val(position.coords.latitude);
        field.find('input[name$="_longitude"]').val(position.coords.longitude);
        $.mobile.loading('hide');
      }, function(error) {
        app.error(error);
        $.mobile.loading('hide');
      }, {
        enableHighAccuracy: true,
        timeout: 10000,
        maximumAge: 0
      });
    } else {
      navigator.notification.alert(i18next.t('location-disabled-alert'), null, i18next.t('location-disabled-header'));
    }
  }, function(error) {
    app.error(error);
  });
};

SurveyView.prototype.getShs = function(field, parent) {
  var self = this;
  var content = $('#shs-select div.ui-content');
  Form.getShs(field.data('shs')).then(function(shs) {
    content.html(Handlebars.templates['shs-select']({
      id: field.data('id'),
      options: shs.value[parent]
    }));
    content.find('ul[data-role="listview"]').listview();
    content.find('ul[data-role="listview"] li a').click(function() {
      if (shs.value[$(this).data('value')]) {
        self.getShs(field, $(this).data('value'));
      } else {
        field.find('input[type="hidden"]').val($(this).data('value'));
        field.find('input[type="text"]').val($(this).text());
        $.mobile.navigate('#survey-fill');
      }
    });
    $.mobile.navigate('#shs-select');
  });
};

SurveyView.prototype.clearShs = function(field) {
  field.find('input[type="hidden"]').val('');
  field.find('input[type="text"]').val('');
};

SurveyView.prototype.clearPage = function() {
  clearTimeout(this.saveTimeout);
  clearInterval(this.saveInterval);
  this.saveInterval = null;
  $(':mobile-pagecontainer').off('pagecontainerbeforechange');
};

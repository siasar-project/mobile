/* global Form, Log, i18next, Survey */

var FormView = function() {
};

FormView.prototype.synchronize = function() {
  var log = new Log($('#sync-log'));
  app.setHeader(i18next.t('synchronize-header'));
  app.closeLeftPanel();
  $.mobile.navigate('#forms-sync');
  Form.synchronize(log).then(function(forms) {
    Survey.synchronize(forms, log).then(function() {
      log.add(i18next.t('finishing-synchronization-message'));
      setTimeout(function() {
        app.home.show();
      }, 5000);
    });
  }).catch(function(error) {
    app.home.show();
    app.error(error);
  });
};

FormView.prototype.list = function() {
  app.closeLeftPanel();
  var forms = Form.list();
  app.setHeader(i18next.t('create-header'));
  if (forms && forms.length) {
    var content = $('#forms-list div.ui-content');
    content.html(Handlebars.templates['forms-list']({
      forms: forms
    }));
    content.find('ul[data-role="listview"]').listview();
    content.find('ul[data-role="listview"] li a').click(function(event) {
      app.survey.open($(event.target).data('id'));
    });
  }
  $.mobile.navigate('#forms-list');
};

FormView.prototype.clear = function() {
  Form.clear();
};

/* global Session, ApiError */

var Api = function() {
};

Api.defaultServer = 'admin';

Api.request = function(url, type, data) {
  var session = Session.get() || new Session();
  var server = session.server || Api.defaultServer;
  var serverUrl = 'http://' + server + '.siasar.org/rest/';
  return new Promise(function(resolve, reject) {
    var config = {
      url: serverUrl + url,
      type: type,
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        'X-CSRF-Token': session.token
      },
      dataType: 'json',
      data: JSON.stringify(data)
    };
    $.ajax(config)
      .done(function(result) {
        session.timestamp = Date.now();
        session.save();
        resolve(result);
      })
      .fail(function(jqxhr) {
        reject(new ApiError(jqxhr.status, jqxhr.responseJSON ?
          jqxhr.responseJSON[0] : jqxhr.status));
      });
  });
};

Api.prepare = function(url, type, data) {
  var session = Session.get();
  return new Promise(function(resolve, reject) {
    if (session && session.isValid()) {
      Api.request(url, type, data).then(resolve).catch(reject);
    } else {
      Session.create(false).then(function() {
        Api.request(url, type, data).then(resolve).catch(reject);
      }).catch(reject);
    }
  });
};

Api.login = function(data) {
  return Api.request('user/login', 'POST', data);
};

Api.logout = function() {
  return new Promise(function(resolve) {
    Api.request('user/logout', 'POST').then(resolve).catch(resolve);
  });
};

Api.token = function() {
  return Api.prepare('user/token', 'POST');
};

Api.getFormsList = function() {
  return Api.prepare('forms', 'GET');
};

Api.getFormFields = function(formId) {
  return Api.prepare('forms/' + formId, 'GET');
};

Api.getGroupStructure = function() {
  return Api.prepare('field_group_structure', 'GET');
};

Api.getTaxonomyTerms = function() {
  return Api.prepare('taxonomy_term', 'GET');
};

Api.getTaxonomyVocabulary = function() {
  return Api.prepare('taxonomy_vocabulary', 'GET');
};

Api.getFieldCollections = function() {
  return Api.prepare('field_collection_structure', 'GET');
};

Api.getVocabulary = function(vocabulary) {
  return Api.prepare('vocab/' + vocabulary, 'GET');
};

Api.getFieldDependencies = function() {
  return Api.prepare('field_dependency_structure', 'GET');
};

Api.sendSurvey = function(data, id) {
  return (id > 0) ? Api.prepare('entity_entityform/' + id, 'PUT', data) : Api.prepare('entity_entityform', 'POST', data);
};

Api.getSurveys = function(type, status, userId) {
  return Api.prepare('entity_entityform?parameters[type]=' + type + '&parameters[field_status]=' + status + '&parameters[field_user_reference]=' + userId, 'GET');
};

Api.sendFile = function(data) {
  return Api.prepare('file', 'POST', data);
};

Api.getFile = function(id) {
  return Api.prepare('file/' + id, 'GET');
};

Api.sendFieldCollection = function(data, id) {
  return (id > 0) ? Api.prepare('entity_field_collection_item/' + id, 'PUT', data) : Api.prepare('custom_field_collection_item', 'POST', data);
};

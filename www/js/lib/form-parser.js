/* global Store, Api, i18next */

var FormParser = function(form, fields, collections, structure, dependencies, cache, log) {
  this.form = form;
  this.fields = fields;
  this.structure = structure;
  this.dependencies = dependencies;
  this.collections = collections;
  this.cache = cache;
  this.log = log;
};

FormParser.prototype.parse = function() {
  var self = this;
  return new Promise(function(resolve, reject) {
    var formLog = self.log.add(i18next.t('processing-form-message') + ' "' + self.form.label + '"...');
    self.parseSections().then(function(sections) {
      self.form.sections = sections;
      self.log.remove(formLog);
      resolve(self.form);
    }).catch(function(error) {
      reject(error);
    });
  });
};

FormParser.prototype.parseSections = function() {
  var self = this;
  return new Promise(function(resolve, reject) {
    var sectionsPromises = [];
    $.each(self.structure.entityform[self.form.id].form, function(groupIndex, group) {
      if (group.parent_name === '') {
        sectionsPromises.push(self.parseGroup(
          self.structure.entityform[self.form.id].form[group.group_name], false));
      }
    });
    Promise.all(sectionsPromises).then(function(parsedSections) {
      resolve(self.sortItems(parsedSections));
    }).catch(function(error) {
      reject(error);
    });
  });
};

FormParser.prototype.parseGroup = function(data, isCollection) {
  var self = this;
  return new Promise(function(resolve, reject) {
    var group = self.parseGroupData(data);
    var childrenPromises = [];
    if (group.children.length) {
      $.each(group.children, function(childIndex, child) {
        if (child.substring(0, 5) === 'group') {
          if (data.children.length) {
            var targetGroup = self.structure[data.entity_type][data.bundle].form[child];
            var targetIsCollection = (data.entity_type === 'field_collection_item');
            childrenPromises.push(self.parseGroup(targetGroup, targetIsCollection));
          }
        } else if (isCollection) {
          if (!self.collections[data.bundle][child]) {
            reject(new Error('Field not found in collection. Form: ' + self.form.label +
              ' - Collection: ' + data.bundle + ' - Field: ' + child));
          }
          childrenPromises.push(self.parseField(self.collections[data.bundle][child]));
        } else {
          if (!self.fields[self.form.id][child]) {
            reject(new Error('Field not found. Form: ' + self.form.label + ' - Field: ' + child));
          }
          childrenPromises.push(self.parseField(self.fields[self.form.id][child]));
        }
      });
    }
    Promise.all(childrenPromises).then(function(parsedChildren) {
      parsedChildren.forEach(function(children, index) {
        group.children[index] = children;
      });
      group.children = self.sortItems(group.children);
      resolve(group);
    });
  });
};

FormParser.prototype.parseGroupData = function(data) {
  return {
    id: data.group_name,
    name: data.group_name,
    label: data.label,
    order: parseInt(data.weight, 10),
    children: data.children,
    type: 'group'
  };
};

FormParser.prototype.parseField = function(data) {
  var self = this;
  var field = self.parseFieldData(data);
  return new Promise(function(resolve, reject) {
    switch (field.module) {
      case 'options':
      case 'entityreference':
        self.parseOptions(data).then(function(options) {
          field.options = options;
          resolve(field);
        });
        break;
      case 'siasar_field_location':
        self.parseMultilevelOptions(data).then(function(options) {
          field.options = options;
          resolve(field);
        }).catch(function(error) {
          reject(error);
        });
        break;
      case 'field_collection':
        self.parseCollection(data).then(function(collection) {
          field.collection = collection;
          resolve(field);
        }).catch(function(error) {
          reject(error);
        });
        break;
      case 'mvf':
        self.parseOptions(data).then(function(options) {
          field.options = options;
          field.range = self.parseRange(data);
          resolve(field);
        });
        break;
      case 'number':
        field.range = self.parseRange(data);
        resolve(field);
        break;
      case 'date':
        self.parseDate(field, data);
        resolve(field);
        break;
      default:
        resolve(field);
    }
  });
};

FormParser.prototype.parseFieldData = function(data) {
  return {
    id: data.field_name,
    name: data.field_name,
    label: data.label,
    order: parseInt(data.widget.weight, 10),
    required: data.required === 1,
    type: data.widget.type,
    multiple: data.widget.cardinality !== '1',
    module: data.display.default.module === 'entityreference' ? 'entityreference' : data.widget.module,
    collectionName: data.entity_type === 'field_collection_item' ? data.bundle : null,
    resource: data.settings.resource,
    reference: this.searchString(data.reference),
    dependencies: this.dependencies[data.field_name] ? this.dependencies[data.field_name] : null,
    description: data.description,
    // FIXME: this shouldn't be hardcoded
    readonly: data.field_name === 'field_pais' || data.field_name === 'field_user_reference'
  };
};

FormParser.prototype.searchString = function(data) {
  if (typeof data !== 'undefined') {
    return data.bundles[0];
  }
  return null;
};

FormParser.prototype.parseDate = function(field, data) {
  if (data.widget.settings.input_format_custom === 'Y') {
    field.module = 'calendar_year';
    field.type = 'number';
    field.range = {
      min: parseFloat(data.widget.settings.year_range.substr(0, 4), 10),
      max: new Date().getFullYear()
    };
  }
};

FormParser.prototype.parseRange = function(data) {
  var range = null;
  var max = null;
  var min = null;
  if (data.widget.type === 'number') {
    max = parseFloat(data.settings.max, 10);
    min = parseFloat(data.settings.min, 10);
  } else {
    max = parseFloat(data.settings.max.value, 10);
    min = parseFloat(data.settings.min.value, 10);
  }
  if (!isNaN(max) || !isNaN(min)) {
    range = {
      max: max,
      min: min
    };
  }
  return range;
};

FormParser.prototype.parseOptions = function(data) {
  var self = this;

  if (data.settings.allowed_values && data.settings.allowed_values[0] &&
    data.settings.allowed_values[0].vocabulary) {
    var vocabulary = data.settings.allowed_values[0].vocabulary;
    if (!self.cache[vocabulary]) {
      self.cache[vocabulary] = new Promise(function(resolve, reject) {
        var downloadLog = self.log.add(i18next.t('downloading-values-message') + ' "' + vocabulary + '"...');
        Api.getVocabulary(vocabulary).then(function(values) {
          self.log.remove(downloadLog);
          var proccessLog = self.log.add(i18next.t('processing-values-message') + ' "' + vocabulary + '"...');
          self.log.remove(proccessLog);
          var optionsObject = $.map(values.allowed_values, function(value, index) {
            return {
              value: index,
              label: value
            };
          });
          if (!data.required && data.widget.cardinality === '1' && data.display.default.module !== 'mvf') {
            optionsObject.push({
              value: null,
              label: i18next.t('unknown-information')
            });
          }
          resolve(optionsObject);
        }).catch(function(error) {
          reject(error);
        });
      });
    }
    return self.cache[vocabulary];
  }

  return new Promise(function(resolve) {
    var optionsObject = $.map(data.settings.allowed_values, function(value, index) {
      return {
        value: index,
        label: value
      };
    });
    if (!data.required && data.widget.cardinality === '1' && data.display.default.module !== 'mvf') {
      optionsObject.push({
        value: null,
        label: i18next.t('unknown-information')
      });
    }
    resolve(optionsObject);
  });
};

FormParser.prototype.parseMultilevelOptions = function(data) {
  var self = this;
  var vocabulary = data.settings.allowed_values[0].vocabulary;
  if (!self.cache[vocabulary]) {
    self.cache[vocabulary] = new Promise(function(resolve, reject) {
      var downloadLog = self.log.add(i18next.t('downloading-values-message') + ' "' + vocabulary + '"...');
      Api.getVocabulary(vocabulary).then(function(shsData) {
        self.log.remove(downloadLog);
        var options = {};
        var proccessLog = self.log.add(i18next.t('processing-values-message') + ' "' + vocabulary + '"...');
        var division;
        $.each(shsData.hierarchy, function(child, parent) {
          if (!options[parent]) {
            options[parent] = [];
          }
          if (shsData.field_codigo_division_admin) {
            division = (shsData.field_codigo_division_admin[child]) ?
            shsData.field_codigo_division_admin[child] : null;
          }

          options[parent].push({
            value: child,
            label: shsData.allowed_values[child],
            administrative_division: division
          });
        });
        Store.getInstance().saveShs(vocabulary, options).then(function() {
          self.log.remove(proccessLog);
          resolve({
            shs: vocabulary,
            forceDeepest: data.widget.settings.siasar_hierarchical_select.force_deepest === '1'
          });
        });
      }).catch(function(error) {
        reject(error);
      });
    });
  }
  return self.cache[vocabulary];
};

FormParser.prototype.parseCollection = function(data) {
  var self = this;
  var childrenPromises = [];
  return new Promise(function(resolve, reject) {
    if (self.structure.field_collection_item[data.field_name]) {
      $.each(self.structure.field_collection_item[data.field_name].form,
        function(childIndex, child) {
          childrenPromises.push(self.parseGroup(child, true));
        });
    } else {
      $.each(self.collections[data.field_name], function(childIndex, child) {
        childrenPromises.push(self.parseField(child));
      });
    }
    Promise.all(childrenPromises).then(function(parsedChildren) {
      resolve(self.sortItems(parsedChildren));
    }).catch(function(error) {
      reject(error);
    });
  });
};

FormParser.prototype.sortItems = function(items) {
  return items.sort(function(a, b) {
    return a.order - b.order;
  });
};

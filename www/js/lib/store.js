/* global */

var Store = function() {
  if (window.openDatabase) {
    this.initDatabase();
  } else {
    throw new Error('Your device/browser does not support WebSQL.');
  }
};

Store.getInstance = function() {
  if (!Store.instance) {
    Store.instance = new Store();
  }
  return Store.instance;
};

Store.prototype.clearForms = function() {
  $.each(localStorage, function(key) {
    if (key !== 'local_user' && key !== 'session') {
      localStorage.removeItem(key);
    }
  });
  this.deleteSurveys();
  this.deleteForms();
  this.deleteShs();
};

Store.prototype.clear = function() {
  localStorage.clear();
};

Store.prototype.saveForms = function(forms) {
  var formsData = forms.map(function(form) {
    return {
      id: form.id,
      label: form.label,
      synchronized: form.synchronized
    };
  });
  localStorage.setItem('forms', JSON.stringify(formsData));
};

Store.prototype.getForms = function() {
  return JSON.parse(localStorage.getItem('forms'));
};


Store.prototype.saveSession = function(session) {
  localStorage.setItem('session', JSON.stringify(session));
};

Store.prototype.getSession = function() {
  return JSON.parse(localStorage.getItem('session'));
};

Store.prototype.saveUser = function(user) {
  localStorage.setItem('local_user', JSON.stringify(user));
};

Store.prototype.getUser = function() {
  return JSON.parse(localStorage.getItem('local_user'));
};

Store.prototype.clearSession = function() {
  localStorage.removeItem('session');
};

Store.prototype.clearLocalUser = function() {
  localStorage.removeItem('local_user');
};

Store.prototype.saveRelationships = function(relations) {
  localStorage.setItem('relationships', JSON.stringify(relations));
};

Store.prototype.getRelationships = function() {
  var relations = JSON.parse(localStorage.getItem('relationships'));
  if (!relations) {
    relations = [];
  }
  return relations;
};

Store.prototype.transaction = function(sql, params, successFunction, errorFunction) {
  this.database.transaction(function(tx) {
    tx.executeSql(sql, params, function(queryTx, result) {
      if (successFunction) successFunction(result);
    }, function(queryTx, error) {
      if (errorFunction) {
        errorFunction(error);
      } else {
        throw error;
      }
    });
  }, function(error) {
    if (errorFunction) {
      errorFunction(error);
    } else {
      throw error;
    }
  });
};

Store.prototype.initDatabase = function() {
  this.database = window.openDatabase('siasapp', '1.0', 'SIASApp DataBase', 10 * 1024 * 1024);
  this.transaction('CREATE TABLE IF NOT EXISTS surveys ( \
    id INTEGER NOT NULL PRIMARY KEY, \
    form_id TEXT NOT NULL, \
    name TEXT, \
    created_date TEXT NOT NULL, \
    updated_date TEXT, \
    data TEXT NOT NULL, \
    uploaded INTEGER NOT NULL, \
    finished INTEGER NOT NULL);', []);

  this.transaction('CREATE TABLE IF NOT EXISTS forms ( \
    id TEXT, \
    label TEXT, \
    sections TEXT, \
    dependencies TEXT);', []);

  this.transaction('CREATE TABLE IF NOT EXISTS shs ( \
    id TEXT, \
    value TEXT);', []);
};

Store.prototype.saveForm = function(form) {
  var self = this;
  return new Promise(function(resolve, reject) {
    var sections = JSON.stringify(form.sections);
    var query;
    var params;
    if (form.sections) {
      query = 'UPDATE forms SET label = ?, sections = ?, dependencies = ? WHERE id = ?';
      params = [form.label, sections, 'dependencies', form.id];

      self.transaction(query, params, function() {
        resolve(form);
      }, function(error) {
        reject(error);
      });
    } else {
      query = 'INSERT INTO forms (id, label, sections, dependencies) VALUES (?,?,?,?)';
      params = [form.id, form.label, sections, 'dependencies'];
      self.transaction(query, params, function() {
        resolve(form);
      }, function(error) {
        reject(error);
      });
    }
  });
};

Store.prototype.getForm = function(id) {
  var self = this;
  return new Promise(function(resolve, reject) {
    var query = 'SELECT * FROM forms WHERE id = ?';
    self.transaction(query, [id], function(result) {
      if (result.rows.length) {
        resolve(self.parseForms(result.rows)[0]);
      } else {
        resolve();
      }
    }, function(error) {
      reject(error);
    });
  });
};

Store.prototype.parseForms = function(rows) {
  var forms = [];
  if (rows.length) {
    for (var i = 0; i < rows.length; i += 1) {
      var row = rows.item(i);
      forms.push({
        id: row.id,
        label: row.label,
        sections: JSON.parse(row.sections)
      });
    }
  }
  return forms;
};

Store.prototype.deleteForms = function() {
  var self = this;
  return new Promise(function(resolve, reject) {
    var query = 'DELETE FROM forms';
    self.transaction(query, [], function() {
      resolve();
    }, function(error) {
      reject(error);
    });
  });
};

Store.prototype.saveSurvey = function(survey) {
  var self = this;
  return new Promise(function(resolve, reject) {
    var currentTime = (new Date()).toJSON();
    var data = JSON.stringify(survey.data);
    var query;
    var params;

    if (survey.id) {
      query = 'UPDATE surveys SET name = ?, updated_date = ?, data = ?, finished = ?, uploaded = ? WHERE id = ?';
      params = [survey.name, currentTime, data, survey.finished ? 1 : 0, survey.uploaded ? 1 : 0,
        survey.id];
      self.transaction(query, params, function() {
        resolve(survey);
      }, function(error) {
        reject(error);
      });
    } else {
      survey.id = (survey.serverId) ? survey.serverId : -Date.now();
      query = 'INSERT INTO surveys (id, form_id, name, created_date, updated_date, data, finished, uploaded) VALUES (?,?,?,?,?,?,?,?)';
      params = [survey.id, survey.formId, survey.name, currentTime, currentTime, data,
        survey.finished ? 1 : 0, survey.uploaded ? 1 : 0];
      self.transaction(query, params, function() {
        resolve(survey);
      }, function(error) {
        reject(error);
      });
    }
  });
};

Store.prototype.updateId = function(survey, serverId) {
  var self = this;
  return new Promise(function(resolve, reject) {
    var query = 'UPDATE surveys SET id = ? WHERE id = ?';
    var params = [serverId, survey.id];
    self.transaction(query, params, function() {
      survey.id = serverId;
      resolve(survey);
    }, function(error) {
      reject(error);
    });
  });
};

Store.prototype.deleteSurvey = function(survey) {
  var self = this;
  return new Promise(function(resolve, reject) {
    var query = 'DELETE FROM surveys WHERE id = ?';
    self.transaction(query, [survey.id], function() {
      resolve();
    }, function(error) {
      reject(error);
    });
  });
};

Store.prototype.deleteSurveys = function() {
  var self = this;
  return new Promise(function(resolve, reject) {
    var query = 'DELETE FROM surveys';
    self.transaction(query, [], function() {
      resolve();
    }, function(error) {
      reject(error);
    });
  });
};

Store.prototype.getSurvey = function(id) {
  var self = this;
  return new Promise(function(resolve, reject) {
    var query = 'SELECT * FROM surveys WHERE id = ?';
    self.transaction(query, [id], function(result) {
      if (result.rows.length) {
        resolve(self.parseSurveys(result.rows)[0]);
      } else {
        resolve();
      }
    }, function(error) {
      reject(error);
    });
  });
};

Store.prototype.getSurveys = function(params) {
  var self = this;
  return new Promise(function(resolve, reject) {
    var query = 'SELECT * FROM surveys';
    var filters = [];
    if (params.formId) {
      filters.push('form_id = ' + params.formId);
    }
    if (typeof params.finished !== 'undefined') {
      filters.push('finished = ' + (params.finished === true ? 1 : 0));
    }
    if (typeof params.uploaded !== 'undefined') {
      filters.push('uploaded = ' + (params.uploaded === true ? 1 : 0));
    }
    if (filters.length) {
      query += ' WHERE ' + filters.join(' AND ');
    }

    self.transaction(query, [], function(result) {
      resolve(self.parseSurveys(result.rows));
    }, function(error) {
      reject(error);
    });
  });
};

Store.prototype.parseSurveys = function(rows) {
  var surveys = [];
  if (rows.length) {
    for (var i = 0; i < rows.length; i += 1) {
      var row = rows.item(i);
      surveys.push({
        id: row.id,
        formId: row.form_id,
        name: row.name,
        createdDate: row.created_date,
        updatedDate: row.updated_date,
        uploaded: row.uploaded === 1,
        finished: row.finished === 1,
        data: JSON.parse(row.data)
      });
    }
  }
  return surveys;
};

Store.prototype.saveShs = function(id, value) {
  var self = this;
  return new Promise(function(resolve, reject) {
    var options = JSON.stringify(value);
    var query;
    var params;
    query = 'INSERT INTO shs (id, value) VALUES (?,?)';
    params = [id, options];
    self.transaction(query, params, function() {
      resolve(id);
    }, function(error) {
      reject(error);
    });
  });
};

Store.prototype.getShs = function(id) {
  var self = this;
  return new Promise(function(resolve, reject) {
    var query = 'SELECT * FROM shs WHERE id = ?';
    self.transaction(query, [id], function(result) {
      if (result.rows.length) {
        resolve(self.parseShs(result.rows)[0]);
      } else {
        resolve();
      }
    }, function(error) {
      reject(error);
    });
  });
};

Store.prototype.parseShs = function(rows) {
  var shs = [];
  if (rows.length) {
    for (var i = 0; i < rows.length; i += 1) {
      var row = rows.item(i);
      shs.push({
        id: row.id,
        value: JSON.parse(row.value)
      });
    }
  }
  return shs;
};

Store.prototype.deleteShs = function() {
  var self = this;
  return new Promise(function(resolve, reject) {
    var query = 'DELETE FROM shs';
    self.transaction(query, [], function() {
      resolve();
    }, function(error) {
      reject(error);
    });
  });
};

Store.prototype.saveFieldDependencies = function(dependencies) {
  localStorage.setItem('field_dependencies', JSON.stringify(dependencies));
};

Store.prototype.getFieldDependencies = function() {
  return JSON.parse(localStorage.getItem('field_dependencies'));
};

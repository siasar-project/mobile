/* global i18next */

var ValidationError = function(field, element) {
  this.element = element;
};
ValidationError.prototype = Object.create(Error.prototype);
ValidationError.prototype.constructor = ValidationError;

var RequiredError = function(field, element) {
  this.message = i18next.t('required-field-message-1') + field.label + i18next.t('required-field-message-2');
  this.title = i18next.t('required-field-title');
  ValidationError.call(this, field, element);
};
RequiredError.prototype = Object.create(ValidationError.prototype);
RequiredError.prototype.constructor = RequiredError;

var OutOfRangeError = function(field, element) {
  this.message = i18next.t('out-of-range-message-1') + field.label + i18next.t('out-of-range-message-2');
  if (field.range.min !== null) {
    this.message += i18next.t('out-of-range-min') + field.range.min + '.';
  }
  if (field.range.max !== null) {
    this.message += i18next.t('out-of-range-max') + field.range.max + '.';
  }
  this.title = i18next.t('out-of-range-title');
  ValidationError.call(this, field, element);
};
OutOfRangeError.prototype = Object.create(ValidationError.prototype);
OutOfRangeError.prototype.constructor = OutOfRangeError;

var NotValidValueError = function(field, element) {
  this.message = i18next.t('not-valid-value-message-1') + field.label + i18next.t('not-valid-value-message-2');
  this.title = i18next.t('not-valid-value-title');
  ValidationError.call(this, field, element);
};
NotValidValueError.prototype = Object.create(ValidationError.prototype);
NotValidValueError.prototype.constructor = NotValidValueError;

var SessionError = function() {
  this.message = this.message || i18next.t('session-error-message');
  this.title = this.title || i18next.t('session-error-title');
};
SessionError.prototype = Object.create(Error.prototype);
SessionError.prototype.constructor = SessionError;

var UserNotDefinedError = function() {
  this.message = i18next.t('user-not-defined');
  SessionError.call(this);
};
UserNotDefinedError.prototype = Object.create(SessionError.prototype);
UserNotDefinedError.prototype.constructor = UserNotDefinedError;

var ApiError = function(status, message) {
  this.message = message || i18next.t('api-error');
  this.status = status;
};
ApiError.prototype = Object.create(Error.prototype);
ApiError.prototype.constructor = ApiError;
